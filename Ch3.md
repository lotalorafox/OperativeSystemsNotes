# Processes 

## Definition

A program in execution, process execution must progress in sequential fashion.

* the stack contain temporary data 
* Program code text section 
* Each program have program counter
* Data section contain global variables
* Heap contain memory dynamically allocated 

Program is an passive entity and a process is an active entity. a program could have many processes.

![Stack](C:\Users\lotal\OneDrive - Universidad Nacional de Colombia\University\fifth semester\OperativeSystems\Notes\img\stack.jpg)



## States IMPORTANT

* **New:** the process is being created

* **Running:** Instructions are being execute 

* **Waiting:** The process is waiting for some event to occur

* **Ready:**  The process is waiting to be assigned to a processor

* **Terminated:** The process has finish execution 

  ![status](../img/status.jpg)


**Scheduler:** local 

**Dispatch:** Close to run 

## Process control block 

Information associated with the process (task control block) and have:

* State 
* Program counter
* CPU register 
* CPU scheduling
* Memory
* Accounting information  
* I/O status 

## Threads

Consider having multiple program counters per process, and multiple location can execute at once.

## Struct 

on Linux us the task_struct 

* ```ps -fea | less   ```  : process status

![struct](../img/struct.jpg)

## Ready and wait queues 

![queues](../img/queues.jpg)

## Process scheduling

![scheduling](../img/processes.jpg)

## CPU switch from process to process 

![context change](../img/context.jpg)

