# notes commands
## touch 
```bash
touch file
```
## mv
```bash
mv file dir
```
## rm
```bash
rm file 
rm dir
## remove recursive
rm -r dir
```
## echo 
print on the screen 
```bash
echo Hola mundo
# enviroment variables
echo $PATH
```
## pwd
pritn the working directory
```bash
pwd 
#/home/estudiante/commands/example
```
## grep
print the lines that match with a expresion
```bash
grep expretion file
grep expretion dir
grep expresion file | more
# word count 
grep expresion dir | wc
```
more info on Mastering Regular Expressions
and Introduction to Automata Theory.

## less
show the output on a better way 
```bash
#example 
grep expresion dir | less
```
## vi
### insert 
```vi
# on line
:i
# at the end 
:a
```
### get out
```vi
# no changes
:q
# out and discart
:q!
# save the changes
:w
#and 
:wq
```vi
## file
know the type of the file
ELf: executable linkable format
```bash
file file.txt
# print the type of the file
```
## find
find files
by 
* name 
* inode 
* others
```bash
find dir -name file -print
```

## head and tail
look the start of the end of the file 
```bash
head -n file | less
tail -n tail | less
```
## sort
order the lines of a file
```bash 
sort file 
```
####note
.file files or dot files is only visibles with ls -al
## variables
save values on the bash
```bash
name=value
echo $name
# value
```
## history
```bash
#history
!file	
```
## input, output and error
[More info](https://www.linuxtechi.com/standard-input-output-error-in-linux/)
#### note 
| use for and on the output of a command
```bash
cat file.txt | wc -l
```
### errors 
```bash
command_options_and_agruments 2>output_file.txt
```
# ps (important !)
snap shot of the actual process 
```bash
ps -fea | less
```
# top
see the process 
```bash
top
# 1 
# show the cpu cores
```
# kill
stop or kill the process 
```kill
kill process
kill -STOP pid
kill -CONT pid
```